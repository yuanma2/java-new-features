package bat.ke.qq.com;

import java.util.Objects;

public class Student {
    private String name;
    private Integer age;
    private double size;
    private double salary;
    public Student() {
    }

    public Student(Integer age) {
        this.age = age;
    }

    public Student(String name, Integer age, double size, double salary) {
        this.name = name;
        this.age = age;
        this.size = size;
        this.salary = salary;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return Double.compare(student.size, size) == 0 &&
                Double.compare(student.salary, salary) == 0 &&
                Objects.equals(name, student.name) &&
                Objects.equals(age, student.age);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age, size, salary);
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", size=" + size +
                ", salary=" + salary +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }
}
