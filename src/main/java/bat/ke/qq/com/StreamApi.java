package bat.ke.qq.com;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.LongStream;
import java.util.stream.Stream;

/**
 * stream
 */
public class StreamApi {
    //需求：学员的年龄大于25的
    List<Student> studentList=  Arrays.asList(
            new Student("a43",18,188,35000),
            new Student("b53",30,188,45000),
            new Student("c66",26,188,55000),
            new Student("d110",24,188,36000),
            new Student("e34",32,188,43000)
    );
    @Test
    public void test(){
        List<String> list=new ArrayList<>();
        Stream<String> stream = list.stream();

        Student[] students=new Student[]{};
        Stream<Student> stream1 = Arrays.stream(students);

        Stream<String> steam=Stream.of("a","b","c");

        Stream<Integer> stream4 = Stream.iterate(0, (x) -> x + 2);
        stream4.limit(10).forEach(System.out::println);
    }

    //终止操作：一次性执行全部内容 --> “惰性求值”
    @Test
    public void test2(){
        Stream<Student> studentStream = studentList.stream()
                .filter((x) -> x.getSalary() > 25000);

        studentStream.forEach(System.out::println);
    }

    //map 映射- 接收Lambda ,将元素转换成其他形式或提取信息。
    // 接收一个函数作为参数,该函数会被应用到每个元素上，并将其映射成一个新 的元素。
    @Test
    public void test3(){
        Stream<String> stream=Stream.of("aaa","bbbb","cccc");
        stream.map((x)->x.toUpperCase())
        .forEach(System.out::println);
        studentList.stream().map(Student::getName).forEach(System.out::println);
    }

    // reduce 归约
    // 可以将流中元素反复结合起来，得到一个值。
    //reduce(T identity, BinaryOperator) / reduce(Binary0perator)
    @Test
    public void  test4(){
        List<Integer> list=Arrays.asList(1,3,5,6,7,9,2);
        Stream<Integer> stream = list.stream();
        Integer reduce = stream.reduce(0, (x, y) -> x + y);
        System.out.println(reduce);
    }

}
