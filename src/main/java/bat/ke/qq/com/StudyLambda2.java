package bat.ke.qq.com;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * 四大函数式接口
 * Lambda 表达理需要“的数式接口”的支持
 * 函数式接口:接口中只有一个拍象方法的接口，称为函数式接口。可以使用注解@functionalnterface修饰
 *
 * 四大内置核心函数式接口：
 *
 * 1、消费型接口   一个参数，无返回值
 *     Consumer<T>
 *     void accept(T t);
 * 2、供给型接口   无参数，有返回值
 *     Supplier<T>
 *     T get();
 * 3、函数型接口  一个参数、有返回值
 *     Function<T, R>
 *     R app1y(T t);
 * 4、断言型接口   有参数，返回值为boolean类型
 *     Predicate<T>
 *     boolean test(T t);
 */
public class StudyLambda2 {
    //消费型Consumer
    //需求:传入一个参数做业务处理，不需要返回值
    public void happy(double money, Consumer<Double> con){
        con.accept(money);
    }

    @Test
    public void test1(){
        happy(1000,(m)-> System.out.println("monkey每次去洗脚，每次消费"+m+"元"));
    }


    //供给型接口 Supplier
    //需求：产生指定数量的整数，放到集合中，返回集合
    public List<Integer> getNumList(int num, Supplier<Integer> sp){
        List<Integer> result=new ArrayList<>();
        for (int i = 0; i < num; i++) {
            result.add(sp.get());
        }
        return result;
    }

    @Test
    public void test2(){
        List<Integer> numList = getNumList(5, () -> (int) (Math.random() * 100));
        numList.forEach(System.out::println);
    }

    //函数型
    //需求2：传入一个字符串，返回一个字符串
    public String strHander(String str, Function<String,String> fun){
        return fun.apply(str);
    }

    @Test
    public void test3(){
        String result = strHander("颜值担当:",(x)->x+"Ant老师");
        System.out.println(result);
    }

    //断言型
    public List<Student> filterStudent3(List<Student> list, Predicate<Student> myPredicate) {
        List<Student> result = new ArrayList<>();
        for (Student student : list) {
            if (myPredicate.test(student)) {
                result.add(student);
            }
        }
        return result;
    }

}
