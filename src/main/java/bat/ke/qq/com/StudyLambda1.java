package bat.ke.qq.com;

//Java8中引入了-个新的操作符"->” 该操作符称为箭头操作符或Lambda 操作符

import org.junit.Test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;

//Java8中引入了一个新的操作符"->” 该操作符称为箭头操作符或Lambda 操作符
//箭头操作符将Lambda表达式拆分成两部分:
// 左侧: Lambda表达式的参数列表
// 右侧: Lambda表达式中所需执行的功能，即Lambda体
// 依赖于函数时接口，Lambda表达式即对接口的实现

//语法1：无参、无返回值
//语法2：有一个参数，无返回值
//语法3：只有一个参数,省略括号
//语法4:有两个以上的参数，有返回值，并且Lambda体中有多条语句
//语法5:Lambda体中只有一条语句，return 和大括号都可以省略不写
//     Lambda表达式的参数列表的数据类型可以省略不写.JVM编译器通过上下文推断数据类型
//语法6:参数指定类型

//上联:左右遇一括号省
//下联:左侧推断类型省
//横批:能省则省

/**
 * 6大语法
 */
public class StudyLambda1 {
    /**
     * 语法1：无参、无返回值
     */
//    Runnable runnable1 = () -> System.out.println("bat.ke.qq.com" + num);
//    runnable1.run();
    @Test
    public void test1() {
        int num = 123; //1.7
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.out.println("bat.ke.qq.com" + num);
            }
        };
    }

    /**
     * 语法2：有一个参数，无返回值
     */
    @Test
    public void test2() {
        Consumer<String> com = (x) -> System.out.println(x);
        com.accept("只为培养bat程序员而生");
    }

    /**
     * 语法3：只有一个参数,省略括号
     */
    @Test
    public void test3() {
        Consumer<String> com = x -> System.out.println(x);
        com.accept("只为培养bat程序员而生");
    }

    /**
     * 语法4:有两个以上的参数，有返回值，并且Lambda体中有多条语句
     */
    @Test
    public void test4() {
        Comparator<Integer> com = (x, y) -> {
            System.out.println("111");
            return Integer.compare(x, y);
        };
    }

    /**
     * 语法5:Lambda体中只有一条语句，return 和大括号都可以省略不写
     */
    @Test
    public void test5() {
        Comparator<Integer> com = (x, y) -> Integer.compare(x, y);
        List<String> list = new ArrayList<>();
    }

    /**
     * 语法6:参数指定类型
     */
    @Test
    public void test6() {
        Comparator<Integer> com = (Integer x, Integer y) -> Integer.compare(x, y);
    }
}
