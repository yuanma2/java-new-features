package bat.ke.qq.com;

public interface MyPredicate<T> {
    boolean test(T t);
}
